import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';

interface Props {
  name: string;
}

interface State {
  users: Array<string>;
}
export default class Example extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      users: [],
    };
  }
  componentDidMount() {
    axios
      .post('/oauth/authorize', {
        client_id: 3,
        grant_type: 'authorization_code',
        client_secret: 'pFN7mdMTRK4M97Ymenq9K0BQmuTaJwVpgzSSLrnA',
      })
      .then(r => console.log(r))
      .catch(e => console.log(e));
  }

  render() {
    return (
      <div className="container">
        <div className="row justify-content-center">
          <div className="col-md-8">
            <div className="card">
              <div className="card-header">Example Component foo bar blah</div>
              <div className="card-body">I&apos;m an example component!</div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

if (document.getElementById('example')) {
  ReactDOM.render(<Example name="hello" />, document.getElementById('example'));
}
