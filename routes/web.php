<?php

use App\User;
use App\Post;
use App\Comment;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/user-factory', function () {
    factory(User::class, 10)
        ->create()
        ->each(function ($user) {
            $user->posts()->save(factory(Post::class)->make())
                ->comments()->save(factory(Comment::class)->make());
        });
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
