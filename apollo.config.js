module.exports = {
  service: {
    name: 'laravel-lighthouse',
    localSchemaFile: './storage/app/lighthouse-schema.graphql',
  },
};
